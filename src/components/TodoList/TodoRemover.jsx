import React from 'react'
import { Glyphicon } from 'react-bootstrap'
import { connect } from 'react-redux'

import { deleteTodo } from '../../actions/todoList'

import './TodoRemover.css'

const TodoRemover = ({todo, deleteTodo}) =>
    <Glyphicon className="todoRemover-removeButton" glyph="remove" onClick={() => deleteTodo(todo)} />

export default connect(
    // mapStateToProps
    null,
    // mapDispatchToProps
    {deleteTodo}
)(TodoRemover)
