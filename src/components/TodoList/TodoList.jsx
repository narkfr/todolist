import React, { Component } from 'react'
import {
    Checkbox,
    ListGroup
} from 'react-bootstrap'

import TodoListItem from './TodoListItem'

import './TodoList.css'

class TodoList extends Component {
    state = {
        displayDone: false
    }

    toggleDisplayDone = () => this.setState({
        displayDone: !this.state.displayDone
    })

    render() {
        const { todoList } = this.props
        const { displayDone } = this.state
        const todoListToDisplay = todoList
            .filter(todo => todo.done === displayDone)
            .reverse()
        return (
            <div className="todoList">
                <div className="todoList-title">
                    <Checkbox checked={displayDone} onClick={() => this.toggleDisplayDone()}>
                        {
                            displayDone
                            ? <h2>Task{todoListToDisplay.length > 1 && 's'} done</h2>
                            : <h2>To do task{todoListToDisplay.length > 1 && 's'}</h2>
                        }
                    </Checkbox>
                </div>
                <ListGroup>
                    {
                        todoListToDisplay.map(
                            todo => <TodoListItem todo={todo} key={todo.createdAt} />
                        )
                    }
                </ListGroup>
            </div>
        )
    }
}

export default TodoList
