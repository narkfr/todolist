import React from 'react'

import CreateOrEditTodo from './CreateOrEditTodo'
import TodoList from './TodoList'
import NoTodo from './NoTodo'

const TodoListWrapper = ({todoList}) => {
    return (
        <main className="todoList-main">
            <CreateOrEditTodo />
            {todoList.length
                ? <TodoList todoList={todoList} />
                : <NoTodo />
            }
        </main>
    )
}

export default TodoListWrapper
