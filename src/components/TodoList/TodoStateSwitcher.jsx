import React from 'react'
import { Checkbox } from 'react-bootstrap'
import { connect } from 'react-redux'

import { editTodo } from '../../actions/todoList'

const TodoStateSwitcher = ({todo, editTodo}) =>
    <Checkbox defaultChecked={todo.done} onClick={() => {
        const doneAt = new Date()
        editTodo({...todo,
            done: !todo.done,
            doneAt: !todo.done ? doneAt.toISOString() : null}
        )}}
    />

export default connect(
    // mapStateToProps
    null,
    // mapDispatchToProps
    {editTodo}
)(TodoStateSwitcher)
