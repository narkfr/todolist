import React, { Component,Fragment } from 'react'
import {
    Badge,
    ListGroupItem,
    Modal
} from 'react-bootstrap'

import formatDate from '../../libs/formatDate'

import CreateOrEditTodo from './CreateOrEditTodo'
import TodoStateSwitcher from './TodoStateSwitcher'
import TodoRemover from './TodoRemover'

import './TodoListItem.css'

class TodoListItem extends Component {
    state = {
        isModalOpen: false
    }
    componentDidMount = () => {
        this.getCatImageUrl()
    }
    getCatImageUrl() {
        fetch('http://aws.random.cat/meow')
            .then(response => response.json())
            .then(data => this.setState({catImageUrl: data}))
            .catch(() => {
                this.setState({catImageUrl: "error"})
            });
    }
    openModal = () => {
        this.setState({ isModalOpen: true })
    }
    closeModal = () => {
        this.setState({ isModalOpen: false })
    }
    render() {
        const { todo } = this.props
        const { isModalOpen, catImageUrl } = this.state
        return (
            <Fragment>
                <ListGroupItem className="todoList-item">
                    <TodoStateSwitcher todo={todo} />
                    <a className="todoList-item-link" onClick={() => this.openModal()}>{todo.description}{todo.doneAt && <i>{` - Done at ${formatDate(todo.doneAt)}`}</i>}</a>
                    <TodoRemover todo={todo} />
                </ListGroupItem>
                <Modal show={isModalOpen} onHide={() => this.closeModal()}>
                    <Modal.Header closeButton>
                        <Modal.Title><Badge>{todo.done ? "Done" : "To do"}</Badge> - {todo.description}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <CreateOrEditTodo edit todo={todo} closeModal={this.closeModal} />
                        <div className="todoList-item-image">
                            {
                                catImageUrl
                                ? catImageUrl === 'error'
                                    ? "oops, something went wrong when fetching a kitten"
                                    : <img src={catImageUrl.file} alt={`Cat for ${todo.description}`} />
                                : null
                            }
                        </div>
                    </Modal.Body>
                    <Modal.Footer />
                </Modal>
            </Fragment>
        )
    }
}


export default TodoListItem
