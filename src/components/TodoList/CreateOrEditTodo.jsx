import React, { Component } from 'react'
import {
    Button,
    Form,
    FormControl,
    FormGroup,
    Glyphicon,
    HelpBlock,
    OverlayTrigger,
    Tooltip
} from 'react-bootstrap'
import { connect } from 'react-redux'

import { addTodo, editTodo } from '../../actions/todoList'
import getRandomTodo from '../../libs/getRandomTodo'

import './CreateOrEditTodo.css'

const tooltip = (
    <Tooltip id="tooltip">
        Generate a funny random todo
    </Tooltip>
)

// This component can have two mode, `edit` or `create`
// default mode is create, to switch to `edit` mode you need to add `edit` as props
class CreateOrEditTodo extends Component {
    state = {
        // we display an error if this value is true to avoid empty to do items
        isValueEmpty: false,
    }

    componentDidMount = () => {
        this.props.edit && this.setState({value: this.props.todo.description})
    }

    submitTodo = (value) => {
        value
            ? this.props.edit ? this.editTodo(value) : this.createAndAddTodo(value)
            : this.setState({ isValueEmpty: true })
    }

    editTodo = (value) => {
        const todo = {
            ...this.props.todo,
            'description': value
        }
        this.props.editTodo(todo)
        this.props.closeModal()
    }

    createAndAddTodo = (value) => {
        const createdAt = new Date()
        const todo = {
            'description': value,
            'createdAt': createdAt.toISOString(),
            'done': false
        }
        this.props.addTodo(todo)
    }

    handleChange = (event) => {
        event.target.value && this.setState({ isValueEmpty: false })
        this.setState({ value: event.target.value })
    }

    handleKeyDown = (event) => {if (event.keyCode === 13) {
        event.preventDefault()
        this.submitTodo(this.state.value)
    }}

    getRandomTodo = () => {
        this.setState({
            value: getRandomTodo(),
            isValueEmpty: false
        })
    }

    render() {
        const { isValueEmpty } = this.state
        const { edit } = this.props
        return (
            <div className="todoList-createOrEdit">
                <h4>{edit ? "Edit this task" : "Add a new task"}</h4>
                <Form inline>
                    <FormGroup validationState={isValueEmpty ? 'error' : null}>
                        <FormControl
                            type="text"
                            placeholder="task description"
                            value={this.state.value}
                            onChange={this.handleChange}
                            onKeyDown={this.handleKeyDown}
                            />
                        {
                            isValueEmpty &&
                            <HelpBlock>You have to make a description to add a task.</HelpBlock>
                        }
                    </FormGroup>

                    <OverlayTrigger placement="top" overlay={tooltip}>
                        <Button bsStyle="default" onClick={this.getRandomTodo}>
                            <Glyphicon glyph="random" />
                        </Button>
                    </OverlayTrigger>

                    <Button bsStyle="primary" onClick={() => this.submitTodo(this.state.value)}>
                        {
                            edit
                            ? "Edit"
                            : "Add"
                        }
                    </Button>
                </Form>
            </div>
        )
    }
}

export default connect(
    // mapStateToProps
    null,
    // mapDispatchToProps
    { addTodo, editTodo }
)(CreateOrEditTodo)
