import React from 'react'
import { Jumbotron } from 'react-bootstrap'

import './NoTodo.css'

const NoTodo = () => <Jumbotron className="noTodo">
    <h4>You don't have any tasks in your to do list.</h4>
</Jumbotron>

export default NoTodo
