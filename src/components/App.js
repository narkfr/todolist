import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'

import { addTodo } from '../actions/todoList'

import Header from './Header/Header'
import TodoListWrapper from './TodoList/TodoListWrapper'

class App extends Component {
    componentWillMount = () => {
        // localStorage set item as string, we have to parse it before adding
        // data to the redux store
        const todoListInLocalStorage = JSON.parse(localStorage.getItem('todoList')) || []
        todoListInLocalStorage.length &&
            todoListInLocalStorage.map(todo => this.props.addTodo(todo))
    }
    render() {
        const { todoList } = this.props.todoList
        return (
            <Fragment>
                <Header />
                <TodoListWrapper todoList={todoList} />
            </Fragment>
    );
  }
}

export default connect(
    // mapStateToProps
    (state) => {
        return { todoList: state.todoList }
    },
    // mapDispatchToProps
    {addTodo}
)(App)
