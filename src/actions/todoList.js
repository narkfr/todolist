export const ADD_TODO = 'ADD_TODO'
export const addTodo = (todo) => {
    return async function (dispatch) {
        dispatch({
            type: ADD_TODO,
            payload: todo
        })
    }
}

export const EDIT_TODO = 'EDIT_TODO'
export const editTodo = (todo) => {
    return async function (dispatch) {
        dispatch({
            type: EDIT_TODO,
            payload: todo
        })
    }
}

export const DELETE_TODO = 'DELETE_TODO'
export const deleteTodo = (todo) => {
    return async function (dispatch) {
        dispatch({
            type: DELETE_TODO,
            payload: todo
        })
    }
}
