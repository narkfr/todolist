import {applyMiddleware, combineReducers, createStore} from 'redux'
import thunk from 'redux-thunk'

import * as todoList from './reducers/todoList'

const middleware = [thunk]

const store = createStore(
    combineReducers({
        ...todoList,
    }),
    applyMiddleware(...middleware)
)


export default store
