import {
    ADD_TODO,
    EDIT_TODO,
    DELETE_TODO
} from '../actions/todoList'

import updateLocalStorageTodoList from '../libs/updateLocalStorageTodoList'

export function todoList(state = {
    todoList: []
}, {
    type,
    payload
}) {
    let newTodolist = []
    switch (type) {
        case ADD_TODO:
            newTodolist = [...state.todoList, payload]
            updateLocalStorageTodoList(newTodolist)
            return {
                ...state,
                todoList: newTodolist
            }
        case EDIT_TODO:
            const todoToEdit = state.todoList.find(todo => todo.createdAt === payload.createdAt)
            newTodolist = [...state.todoList]
            newTodolist[state.todoList.indexOf(todoToEdit)] = payload
            updateLocalStorageTodoList(newTodolist)
            return {
                ...state,
                todoList: newTodolist
            }
        case DELETE_TODO:
            const indexOfTodoToRemove = state.todoList.indexOf(
                state.todoList.find(todo => todo.createdAt === payload.createdAt)
            )
            newTodolist = [
                ...state.todoList.slice(0, indexOfTodoToRemove),
                ...state.todoList.slice(indexOfTodoToRemove + 1)
            ]
            updateLocalStorageTodoList(newTodolist)
            return {
                ...state,
                todoList: newTodolist
            }

        default:
            return state
    }
}
