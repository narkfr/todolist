// Date are stocked as string in localStorage, to display them nicely we need to
// the string to a Date object
const formatDate = (date) => {
    const realDateObject = new Date(date)
    return `
        ${realDateObject.getDay()}/${realDateObject.getMonth()+1}/${realDateObject.getFullYear()} at
        ${realDateObject.getHours()}h${realDateObject.getMinutes()}`
}

export default formatDate
