// localStorage is updated after every redux action, to keep date consistent
// between the redux store and user session.
// As we have objects in the redux store, we stringify them before setting the
// todoList array in localStorage
const updateLocalStorageTodoList = (todoList) =>
    localStorage.setItem('todoList', JSON.stringify(todoList))

export default updateLocalStorageTodoList
