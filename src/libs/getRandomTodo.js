// This lib generate a random `todo` description, based on ACTIONS and OBJECTS
// array in this format : (TODO = <ACTION> <OBJECT>)
const ACTIONS = [
    'eat',
    'sleep',
    'sell',
    'buy',
    'destroy',
    'throw',
    'bury',
]

const OBJECTS = [
    'the banana',
    'the dog',
    'a fireman',
    'a dancing guy',
    'Station F',
    'the coffin',
]

const getRandomTodo = () => `${ACTIONS[Math.floor(Math.random()*ACTIONS.length)]} ${OBJECTS[Math.floor(Math.random()*OBJECTS.length)]}`

export default getRandomTodo
