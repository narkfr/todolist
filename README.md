# ToDo List

ToDo List is a to do list app.

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

The project is published on [nark.fr/todo](http://nark.fr/minisshub/)

## Purposes

I use Create React to focus on the coding style, and avoid a long tooling configuration. A webpack configuration is specific for each project, and this exercice doesn't need a specific setup.

Bootstrap is used as CSS framework.

The **Redux** is used as one and only one source of truth to fetch and store repositories and issues, and persist data during navigation.

The **LocalStorage** is used to persist data between session.

## Install

__Requirements__


You'll need to have Node >=6 on your local development machine, and Yarn.

``yarn install``: install the project
``yarn start``: run the project. It will open a browser with your project.

All default Create React App scripts are supported, see [Create React App README](https://github.com/facebook/create-react-app/blob/next/README.md) for full documentation
